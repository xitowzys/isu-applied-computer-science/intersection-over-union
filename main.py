def iou(bbox1, bbox2) -> float:
    x1 = max(bbox1[0], bbox2[0])
    y1 = min(bbox1[1], bbox2[1])
    x2 = max(bbox1[2], bbox2[2])
    y2 = min(bbox1[3], bbox2[3])

    if (x1 > y1 or x2 > y2):
        return 0

    interArea = (y1 - x1) * (y2 - x2)

    area1 = (bbox1[1] - bbox1[0]) * (bbox1[3] - bbox1[2])
    area2 = (bbox2[1] - bbox2[0]) * (bbox2[3] - bbox2[2])

    # print(f"Areas: {area1, area2}")

    iou = interArea / (area1 + area2 - interArea)

    return iou

bbox1 = [0, 10, 0, 10]
bbox2 = [0, 10, 1, 10]
bbox3 = [20, 30, 20, 30]
bbox4 = [5, 15, 5, 15]

assert iou(bbox1, bbox1) == 1.0
assert iou(bbox1, bbox2) == 0.9
assert iou(bbox1, bbox3) == 0.0
assert round(iou(bbox1, bbox4), 2) == 0.14