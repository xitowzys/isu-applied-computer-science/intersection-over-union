# Intersection over Union

## Задание
Написать функцию для расчета IOU. На вход подается два bounding box.

```python
def iou(bbox1: list, bbox2: list) -> float:
    pass

bbox1 = [0, 10, 0, 10]
bbox2 = [0, 10, 1, 10]
bbox3 = [20, 30, 20, 30]
bbox4 = [5, 15, 5, 15]
assert iou(bbox1, bbox1) == 1.0
assert iou(bbox1, bbox2) == 0.9
assert iou(bbox1, bbox3) == 0.0
assert round(iou(bbox1, bbox4), 2) == 0.14
```